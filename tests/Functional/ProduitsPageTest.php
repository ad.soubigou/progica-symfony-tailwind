<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProduitsPageTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/produits');

        $this->assertResponseIsSuccessful();

        $button = $crawler->selectLink("Connexion");
        $this->assertEquals(1, count($button));
    }
}
