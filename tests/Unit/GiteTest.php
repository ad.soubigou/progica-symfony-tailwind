<?php

namespace App\Tests\Unit;

use App\Entity\Gite;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GiteTest extends KernelTestCase
{
    public function testEntityIsValid(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        $gite = new Gite();
        $gite->setNom('Gite 1')
            ->setAdresse('Adresse 1')
            ->setCodePostal('00000')
            ->setSurface(1.5)
            ->setNbChambre(1)
            ->setNbCouchage(1)
            ->setCapacite(1)
            ->setDescription('description')
            ->setNbAnimaux(1);

        $errors = $container->get('validator')->validate($gite);

        $this->assertCount(0, $errors);
    }
}
