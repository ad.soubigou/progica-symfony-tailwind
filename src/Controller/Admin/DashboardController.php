<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use App\Entity\Departement;
use App\Entity\DetailsEquipement;
use App\Entity\DetailsService;
use App\Entity\Equipement;
use App\Entity\Gite;
use App\Entity\Photos;
use App\Entity\Proprietaire;
use App\Entity\Region;
use App\Entity\Saison;
use App\Entity\Service;
use App\Entity\Ville;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(GiteCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Progica Dashboard');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Gites', 'fa fa-home', Gite::class);

        yield MenuItem::section('Personnes');
        yield MenuItem::linkToCrud('Propriétaires', 'fa-solid fa-person', Proprietaire::class);
        yield MenuItem::linkToCrud('Contacts', 'fa-solid fa-address-book', Contact::class);

        yield MenuItem::section('Localisation');
        yield MenuItem::linkToCrud('Villes', 'fa-solid fa-city', Ville::class);
        yield MenuItem::linkToCrud('Départements', 'fa-regular fa-map', Departement::class);
        yield MenuItem::linkToCrud('Régions', 'fa-solid fa-map', Region::class);

        yield MenuItem::section('Autres');
        yield MenuItem::linkToCrud('Equipements', 'fa-solid fa-bath', Equipement::class);
        yield MenuItem::linkToCrud('Services', 'fa-solid fa-wifi', Service::class);
        yield MenuItem::linkToCrud('Saisons', 'fa-solid fa-sun', Saison::class);
        yield MenuItem::linkToCrud('Photos', 'fa-solid fa-image', Photos::class);
        // yield MenuItem::linkToCrud('DétailsEquipements', 'fa fa-home', DetailsEquipement::class);
        // yield MenuItem::linkToCrud('DétailsServices', 'fa fa-home', DetailsService::class);
    }
}
