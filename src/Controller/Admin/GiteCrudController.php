<?php

namespace App\Controller\Admin;

use App\Entity\Gite;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class GiteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Gite::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Administration des Gites');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('nom'),
            AssociationField::new('proprietaire'),
            AssociationField::new('contact'),
            TextField::new('adresse'),
            TextField::new('code_postal'),
            AssociationField::new('ville'),
            AssociationField::new('departement'),
            AssociationField::new('region'),
            AssociationField::new('saisons'),
            AssociationField::new('photos'),
            // AssociationField::new('detailsEquipements'),
            NumberField::new('surface'),
            IntegerField::new('nb_chambre'),
            IntegerField::new('nb_couchage'),
            IntegerField::new('capacite'),
            IntegerField::new('nb_animaux'),
            TextEditorField::new('description')
                ->hideOnIndex(),

        ];
    }
}
