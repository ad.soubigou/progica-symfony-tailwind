<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Gite;
use App\Form\SearchForm;
use App\Repository\GiteRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Request;

#[Route('/produits', name: 'produits_')]
class ProduitsController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(GiteRepository $giteRep, Request $request): Response
    {

        $jour = new DateTime();

        // Récupération des données de recherche
        $data = new SearchData();
        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);
        $gites = $giteRep->findSearch($data);

        return $this->render(
            'produits/index.html.twig',
            [
                'gite' => $gites,
                'jour' => $jour,
                'form' => $form->createView()
            ]
        );
    }

    #[Route('/{id}', name: 'details')]
    public function details(Gite $gite): Response
    {
        $saison = $gite->getSaisons();
        $photos = $gite->getPhotos();
        $jour = new DateTime();
        return $this->render('/produits/details.html.twig', [
            'gite' => $gite,
            'saisons' => $saison,
            'photos' => $photos,
            'jour' => $jour
        ]);
    }
}
