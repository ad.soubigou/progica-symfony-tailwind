<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Gite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Gite>
 */
class GiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gite::class);
    }

    /**
     * Récupère les gites en lien avec la recherche
     * @return Gites[]
     */
    public function findSearch(SearchData $search)
    {
        $query = $this
            ->createQueryBuilder('gites')
            ->join('gites.ville', 'villes')
            ->join('gites.departement', 'departements')
            ->join('gites.region', 'regions')
            ->join('gites.detailsEquipements', 'equipements')
            ->join('gites.detailsServices', 'services')
            ->join('gites.saisons', 'saisons');

        if (!empty($search->q)) {
            $query = $query
                ->andWhere('villes.nom LIKE :q OR departements.nom LIKE :q OR regions.nom LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }

        if (!empty($search->equipements)) {
            $query = $query
                ->andWhere('equipements.equipement IN (:equipements)')
                ->setParameter('equipements', $search->equipements);
        }

        if (!empty($search->services)) {
            $query = $query
                ->andWhere('services.service IN (:services)')
                ->setParameter('services', $search->services);
        }

        if (!empty($search->min)) {
            $query = $query
                ->andWhere('saisons.tarif_hebdo >= :min')
                ->setParameter('min', $search->min);
        }

        if (!empty($search->max)) {
            $query = $query
                ->andWhere('saisons.tarif_hebdo <= :max')
                ->setParameter('max', $search->max);
        }

        return $query->getQuery()->getResult();
    }
}
