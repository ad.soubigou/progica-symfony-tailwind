<?php

namespace App\Form;

use App\Data\SearchData;
use App\Entity\Equipement;
use App\Entity\Service;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Où souhaitez-vous aller ?',
                    'class' => 'w-1/4 rounded'
                ]
            ])
            ->add('equipements', EntityType::class, [
                'label' => false,
                'required' => false,
                'class' => Equipement::class,
                'expanded' => true,
                'multiple' => true,
                'attr' => [
                    'class' => 'p-2'
                ]
            ])
            ->add('services', EntityType::class, [
                'label' => false,
                'required' => false,
                'class' => Service::class,                'expanded' => true,
                'multiple' => true,
                'attr' => [
                    'class' => 'p-2'
                ]
            ])
            ->add('min', NumberType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Prix min.',
                    'class' => 'rounded w-24',
                ]
            ])
            ->add('max', NumberType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Prix max.',
                    'class' => 'rounded w-24'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                // La classe qui gère nos données de recherche
                'data_class' => SearchData::class,
                // La méthode utilisée
                'method' => 'GET',
                'crsf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
