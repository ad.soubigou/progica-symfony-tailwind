<?php

namespace App\Data;

class SearchData
{
    /**
     * @var string
     */
    public $q = '';

    /**
     * Equipement[]
     */
    public $equipements = [];

    /**
     * Service[]
     */
    public $services = [];


    /**
     * @var ?int
     */
    public $max;

    /**
     * @var ?int
     */
    public $min;
}
